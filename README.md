# "No More Spiders"

A client-sided reskin overhaul of the spiders in Don't Starve Together to be more arachnaphobia friendly, for real this time.
Working title is 'No More Spiders'

Download the current copy by selecting the download button (the bracket with the down pointing arrow after 'WEB IDE' & 'Find file') and select zip to download the mod!
Add the folder within the zip to your mods folder (C:\Program Files (x86)\Steam\SteamApps\common\Don't Starve Together\mods) to get play testing!

![image](https://i.imgur.com/jrZ5sn3.png)
