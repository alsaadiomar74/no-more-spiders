name = "No More Spiders"-- Placeholder
description = "(Gitlab Vers.)\n\n all spiders to be tribble-like balls"-- Placeholder
author = "Robot Pizza Party"
version = "0"

api_version = 10
priority = 1
server_filter_tags = {"arachnophobia friendly"}

dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false

all_clients_require_mod = false

client_only_mod = true

server_only_mod = false

server_only = false

icon_atlas = "images/modicon.xml"
icon = "modicon.tex"

local function Breaker(name, label, hover, options, default)
    return {name = name, label = label, hover = hover or "", options = options, default = default}
end

local function Title(title)
    return Breaker(title, title, "", {{description = "", data = 0}}, 0)
end

configuration_options =
{
	Title("Spider"),-- TODO: Eventually have configs for each spider thing separate
	{
        name = "spider_noises",
        label = "Spider Noises",
		hover = "Replace the sounds of the Spider with other animal noises.",
        options = 
        {
            {description = "Yes", data = "y"},
			{description = "No", data = "n"},
        }, 
        default = "n",
    },

}
