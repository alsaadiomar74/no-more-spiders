local STRINGS = GLOBAL.STRINGS

Assets = {
	--base spider
	Asset("ANIM", "anim/spider_yarn_build.zip"),
	Asset("ATLAS", "images/inventoryimages/spider_yarn.xml"),
	Asset("IMAGE", "images/inventoryimages/spider_yarn.tex"),
	--Placeholder
	Asset("IMAGE", "images/minimap/esentry.tex"),
	Asset("ATLAS", "images/minimap/esentry.xml"),

	--caves 1
	Asset ("ANIM", "anim/ds_spider_caves.zip"),

	--spitter
	Asset ("ANIM", "anim/ds_spider2_caves.zip"),

	--moon
	Asset ("ANIM", "anim/ds_spider_moon.zip"),

	--spider queen
	Asset ("ANIM", "anim/spider_queen_build.zip"),

	--spider warrior
	Asset ("ANIM", "anim/spider_yarn_warrior_build.zip"),
	Asset("ATLAS", "images/inventoryimages/spider_yarn_warrior.xml"),
	Asset("IMAGE", "images/inventoryimages/spider_yarn_warrior.tex"),

	--spider hat
--	Asset ("ANIM", "anim/hat_spider.zip"),

	--spider water
	Asset ("ANIM", "anim/spider_water.zip"),

	--dangler
	Asset ("ANIM", "anim/spider_yarn_white_build.zip"),
	Asset("ATLAS", "images/inventoryimages/spider_yarn_white.xml"),
	Asset("IMAGE", "images/inventoryimages/spider_yarn_white.tex"),

	--nurse
	Asset ("ANIM", "anim/spider_wolf_build.zip"),

	--spider dens
	Asset ("ANIM", "anim/spider_cocoon.zip"),
	Asset ("ANIM", "anim/spider_mound.zip"),
	Asset ("ANIM", "anim/spider_yarn_mound_mutated.zip"),

	--spider items
	Asset ("ANIM", "anim/spider_egg_sac.zip"),

	--web creep
	Asset("IMAGE", "levels/textures/placeholder_noise.tex"),
}

local resolvefilepath = GLOBAL.resolvefilepath


RegisterInventoryItemAtlas(resolvefilepath("images/inventoryimages/spider_yarn.xml"), "spider.tex")
RegisterInventoryItemAtlas(resolvefilepath("images/inventoryimages/spider_yarn_warrior.xml"), "spider_warrior.tex")
RegisterInventoryItemAtlas(resolvefilepath("images/inventoryimages/spider_yarn_white.xml"), "spider_dropper.tex")

--Spider Creep

local function GroundNoise(name)
    local trimmed_name = name:gsub("%.tex$", "")..".tex"
    if GLOBAL.softresolvefilepath(trimmed_name, true) then
        return resolvefilepath(trimmed_name, true)
    end
    return resolvefilepath("levels/textures/"..trimmed_name, true)
end

local TileManager = require("tilemanager")
TileManager.SetGroundCreepProperty(GLOBAL.GROUND_CREEP_IDS.WEBCREEP, "noise_texture", GroundNoise("placeholder_noise.tex"))

-- Spider Common

AddPrefabPostInit("spider", function(inst)
	inst.AnimState:SetBuild("spider_yarn_build")
end)

-- Strings (Examples)

--TOOO, Hornet: The describe strings will not work as a client mod.

STRINGS.NAMES.SPIDER = "Not a Spider"-- Placeholder
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SPIDER = 
{
			DEAD = "Ewwww!",
			GENERIC = "I hate those things.",
			SLEEPING = "I'd better not be here when he wakes up.",
}
STRINGS.CHARACTERS.WEBBER.DESCRIBE.SPIDER = 
{
			DEAD = "Another lost friend.",
			GENERIC = "Yarn understand us.",
			SLEEPING = "Aww! I think it's having a dream.",
}

-- Spider Warrior

AddPrefabPostInit("spider_warrior", function(inst)
	inst.AnimState:SetBuild("spider_yarn_warrior_build")
end)

-- Spider Hider

-- Spider Spitter

-- Spider Dropper

AddPrefabPostInit("spider_dropper", function(inst)
	inst.AnimState:SetBuild("spider_yarn_white_build")
end)

-- Spider Moon

-- Spider Healer

-- Spider Water

-- Spider Queen

-- Spider Webber

-- Spider Den
-- TODO: Den Minimap icons (we have to hook into its stages U_U)
AddPrefabPostInit("moonspiderden", function(inst)
	inst.AnimState:SetBank("spider_yarn_mound_mutated")-- Changing bank too, otherwise they didn't work right
	inst.AnimState:SetBuild("spider_yarn_mound_mutated")
end)

local SPIDERDEN_TAGS = {"spiderden"}
AddPrefabPostInit("mining_fx", function(inst)
	--Hornet: Crappy hack due to this being a client mod. The mining effects are spawned server side so we have to
	--spawn the moon glass fx upon initilization of the normal rock fx and immediately remove it.
	--My way of checking if we should actually replace the effect is a bit hacky too, and not particuarly great.

	inst:DoTaskInTime(0, function() --leave a frame of time for the effect to be properly placed
		local moonDen = GLOBAL.GetClosestInstWithTag(SPIDERDEN_TAGS, inst, .1)
		if not moonDen then
			return
		end

		if moonDen:GetPosition() == inst:GetPosition() then
	 		GLOBAL.ReplacePrefab(inst, "mining_moonglass_fx")
		end
	end)
end)

-- Spider Everything Else


--Minimap Example
--[[
AddMinimapAtlas("images/minimap/esentry.xml")

AddPrefabPostInit("spider", function(inst)
	inst.MiniMapEntity:SetIcon("esentry.tex")
end)--]]


--Remapping sound

--Spider sounds
-- TODO:
if GetModConfigData("spider_noises") == "y" then
RemapSoundEvent( "dontstarve/creatures/spider", "dontstarve/creatures/smallbird" )
RemapSoundEvent( "dontstarve/creatures/spider/Attack", "dontstarve/creatures/smallbird/attack" )
RemapSoundEvent( "dontstarve/creatures/spider/attack_grunt", "dontstarve/creatures/chester/pant" )
RemapSoundEvent( "dontstarve/creatures/spider/descend", "dontstarve/creatures/smallbird/leg_sproing" )
RemapSoundEvent( "dontstarve/creatures/spider/die", "dontstarve/creatures/chester/death" )
RemapSoundEvent( "dontstarve/creatures/spider/eat", "dontstarve/creatures/chester/chomp" )
--RemapSoundEvent( "dontstarve/creatures/spider/fallAsleep", "dontstarve/creatures/smallbird/sleep" )
RemapSoundEvent( "dontstarve/creatures/spider/hit", "dontstarve/creatures/pengull/hurt" )
RemapSoundEvent( "dontstarve/creatures/spider/hit_response", "dontstarve/creatures/smallbird/hurt" )
RemapSoundEvent( "dontstarve/creatures/spider/scream", "dontstarve/creatures/smallbird/chirp" )
--RemapSoundEvent( "dontstarve/creatures/spider/sleeping", "dontstarve/creatures/smallbird/sleep" )
--RemapSoundEvent( "dontstarve/creatures/spider/spiderExitLair", "dontstarve/creatures/spider/spiderExitLair" )
--RemapSoundEvent( "dontstarve/creatures/spider/spiderLair_destroy", "dontstarve/creatures/spider/spiderLair_destroy" )
--RemapSoundEvent( "dontstarve/creatures/spider/spiderLair_grow", "dontstarve/creatures/spider/spiderLair_grow" )
--RemapSoundEvent( "dontstarve/creatures/spider/spiderLair_hit", "dontstarve/creatures/spider/spiderLair_hit" )
--RemapSoundEvent( "dontstarve/creatures/spider/spider_egg_sack", "dontstarve/creatures/spider/spider_egg_sack" )
--RemapSoundEvent( "dontstarve/creatures/spider/spidernest_LP", "dontstarve/creatures/spider/spidernest_LP" )
--RemapSoundEvent( "dontstarve/creatures/spider/wakeUp", "dontstarve/creatures/smallbird/wakeup" )
RemapSoundEvent( "dontstarve/creatures/spider/walk_spider", "dontstarve/creatures/chester/boing" )

--Spider Queen

--RemapSoundEvent( "dontstarve/creatures/spiderqueen", "dontstarve/creatures/spiderqueen" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/Attack", "dontstarve/creatures/teenbird/attack" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/attack_grunt", "dontstarve/frog/grunt" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/die", "dontstarve/creatures/tallbird/death" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/distress", "dontstarve/creatures/teenbird/hurt" )
--RemapSoundEvent( "dontstarve/creatures/spiderqueen/eat", "dontstarve/creatures/spiderqueen/eat" )
--RemapSoundEvent( "dontstarve/creatures/spiderqueen/emerge_foley", "dontstarve/creatures/spiderqueen/emerge_foley" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/emerge_voice", "dontstarve/creatures/teenbird/leg_sproing" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/fallAsleep", "dontstarve/creatures/tallbird/sleep" )
--RemapSoundEvent( "dontstarve/creatures/spiderqueen/givebirth_foley", "dontstarve/creatures/spiderqueen/givebirth_foley" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/givebirth_voice", "dontstarve/creatures/smallbird/grow" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/hit", "dontstarve/creatures/teenbird/scratch_ground" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/hit_response", "dontstarve/creatures/teenbird/chirp" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/hurt", "dontstarve/creatures/teenbird/hurt" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/legBurst", "dontstarve/creatures/teenbird/wings" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/scream", "dontstarve/creatures/tallbird/chirp" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/scream_short", "dontstarve/creatures/teenbird/chirp_short" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/sleeping", "dontstarve/creatures/rook_minotaur/sleep" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/swipe", "dontstarve/creatures/rocklobster/clawsnap" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/wakeUp", "dontstarve/creatures/tallbird/wakeup" )
RemapSoundEvent( "dontstarve/creatures/spiderqueen/walk_spiderqueen", "dontstarve/creatures/rook/bounce" )

--Spider Warrior

--RemapSoundEvent( "dontstarve/creatures/spiderwarrior", "dontstarve/creatures/spiderwarrior" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/Attack", "dontstarve/creatures/teenbird/attack" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/attack_grunt", "dontstarve/creatures/teenbird/chirp_short" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/die", "dontstarve/creatures/teenbird/death" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/eat", "dontstarve/creatures/teenbird/swallow" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/fallAsleep", "dontstarve/creatures/teenbird/sleep" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/hit", "dontstarve/creatures/teenbird/peck" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/hit_response", "dontstarve/creatures/teenbird/chirp_short" )
--RemapSoundEvent( "dontstarve/creatures/spiderwarrior/jump", "dontstarve/creatures/spiderwarrior/jump" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/scream", "dontstarve/creatures/teenbird/chirp" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/sleeping", "dontstarve/creatures/teenbird/sleep" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/wakeUp", "dontstarve/creatures/teenbird/wakeup" )
RemapSoundEvent( "dontstarve/creatures/spiderwarrior/walk_spider", "dontstarve/creatures/chester/boing" )

--Cave Spider

--RemapSoundEvent( "dontstarve/creatures/cavespider", "dontstarve/creatures/cavespider" )
RemapSoundEvent( "dontstarve/creatures/cavespider/Attack", "dontstarve/creatures/teenbird/attack" )
RemapSoundEvent( "dontstarve/creatures/cavespider/attack_grunt", "dontstarve/creatures/teenbird/chirp_short" )
RemapSoundEvent( "dontstarve/creatures/cavespider/die", "dontstarve/creatures/teenbird/death" )
RemapSoundEvent( "dontstarve/creatures/cavespider/eat", "dontstarve/creatures/teenbird/swallow" )
RemapSoundEvent( "dontstarve/creatures/cavespider/fallAsleep", "dontstarve/creatures/teenbird/sleep" )
RemapSoundEvent( "dontstarve/creatures/cavespider/hide", "dontstarve/creatures/rocklobster/hide" )
RemapSoundEvent( "dontstarve/creatures/cavespider/hit", "dontstarve/creatures/teenbird/peck")
RemapSoundEvent( "dontstarve/creatures/cavespider/hit_response", "dontstarve/creatures/teenbird/chirp_short" )
--RemapSoundEvent( "dontstarve/creatures/cavespider/jump", "dontstarve/creatures/cavespider/jump" )
RemapSoundEvent( "dontstarve/creatures/cavespider/scream", "dontstarve/creatures/teenbird/chirp" )
RemapSoundEvent( "dontstarve/creatures/cavespider/sleeping", "dontstarve/creatures/teenbird/sleep" )
RemapSoundEvent( "dontstarve/creatures/cavespider/spit_voice", "dontstarve/creatures/rocklobster/taunt" )
RemapSoundEvent( "dontstarve/creatures/cavespider/spit_web", "dontstarve/creatures/rocklobster/attack_whoosh" )
RemapSoundEvent( "dontstarve/creatures/cavespider/wakeUp", "dontstarve/creatures/teenbird/wakeup" )
RemapSoundEvent( "dontstarve/creatures/cavespider/walk_spider", "dontstarve/creatures/smallbird/footstep" )
end
